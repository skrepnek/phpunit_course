<?php

use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    use CustomAssertionTrait;

    // Private and protected properties
    public function testValidUserName()
    {
        $user = new User('joe', 'Doe');
        $expected = 'Joe';
        $phpunit = $this;
        $closure = function () use ($phpunit, $expected) {
            $phpunit->assertSame($expected, $this->name);
        };

        $binding = $closure->bindTo($user, get_class($user));
        $binding();
    }

    // Protected property only
    public function testValidUserName2()
    {
        $user = new class('joe', 'doe') extends User
        {
            public function getName()
            {
                return $this->name;
            }
        };

        $expected = 'Joe';
        $this->assertSame($expected, $user->getName());
    }

    public function testVAlidDataFormat()
    {
        $user = new User('joe', 'Doe');
        $mockedDb = new class extends Database
        {
            public function getEmailAndLastName()
            {
                echo 'no real db touched!';
            }
        };

        $setUserClosure = function () use ($mockedDb) {
            $this->db = $mockedDb;
        };
        $executeSetUserClosure = $setUserClosure->bindTo($user, get_class($user));
        $executeSetUserClosure();

        $this->assertSame('Joe Doe', $user->getFullName());
    }

    // For private and protected methods
    public function testPasswordHashed()
    {
        $user = new User('joe', 'Doe');
        $expected = 'password hashed!';
        $phpunit = $this;

        $assertClosure = function () use ($phpunit, $expected) {
            /** @var User $this */
            /** @var TestCase $phpunit */
            $phpunit->assertSame($expected, $this->hashPassword());
        };

        $executeAssertClosure = $assertClosure->bindTo($user, get_class($user));
        $executeAssertClosure();
    }

    // For private methods only
    public function testPasswordHashed2()
    {
        $user = new class('joe', 'Doe') extends User
        {
            public function getHashedPassword()
            {
                return $this->hashPassword();
            }
        };

        $this->assertSame('password hashed!', $user->getHashedPassword());
    }

    public function testCustomDataStructure()
    {
        $data = [
            'nick' => 'JD',
            'email' => 'joe.doe@test.com',
            'age' => 55
        ];
        $this->assertArrayData($data);
    }

    public function testSomeOperation()
    {
        $user = new User('joe', 'Doe');

        $this->assertEquals('ok!', $user->someOperation([1, 2, 3]));
        $this->assertEquals('ok!', $user->someOperation([1]));
        $this->assertEquals('error', $user->someOperation([0]));
        $this->assertEquals('error', $user->someOperation([]));
    }
}
