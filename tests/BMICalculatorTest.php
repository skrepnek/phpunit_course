<?php

use PHPUnit\Framework\TestCase;

class BMICalculatorTest extends TestCase
{
    public function testShowsUnderweightWhenBmiLessThan18()
    {
        $BMICalculator = new BMICalculator();
        $BMICalculator->BMI = 10;

        $result = $BMICalculator->getTextResultFromBMI();
        $expected = 'Underweight';
        $this->assertSame($expected, $result);
    }

    public function testShowsNormalWhenBmiBetween18and25()
    {
        $BMICalculator = new BMICalculator();
        $BMICalculator->BMI = 24;

        $result = $BMICalculator->getTextResultFromBMI();
        $expected = 'Normal';
        $this->assertSame($expected, $result);
    }

    public function testShowsOverweightWhenBmiOver25()
    {
        $BMICalculator = new BMICalculator();
        $BMICalculator->BMI = 33;

        $result = $BMICalculator->getTextResultFromBMI();
        $expected = 'Overweight';
        $this->assertSame($expected, $result);
    }

    public function testCanCalculateCorrectBmi()
    {
        $expected = 39.1;
        $BMICalculator = new BMICalculator();
        $BMICalculator->mass = 100; //kg
        $BMICalculator->height = 1.6; //m

        $result = $BMICalculator->calculate();
        $this->assertEquals($expected, $result);
        $this->assertSame(BASEURL, 'http://localhost2');
    }
}
