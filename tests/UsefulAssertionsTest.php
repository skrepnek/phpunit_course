<?php

use PHPUnit\Framework\TestCase;

class UsefulAssertionsTest extends TestCase
{
    public function testAssertSame() // For strings only. For numbers use assertEquals
    {
        $expect = 'bar';
        $actual = 'bar';
        $this->assertSame($expect, $actual);
    }

    public function testAssertEquals() // For numbers
    {
        $expect = 1;
        $actual = 1;
        $this->assertEquals($expect, $actual);
    }

    public function testAssertEmpty() //Only for arrays
    {
        $this->assertEmpty([]);
    }

    public function testAssertNull()
    {
        $this->assertNull(null);
    }

    public function testAssertGreaterThan()
    {
        $expect = 2;
        $actual = 3;
        $this->assertGreaterThan($expect, $actual);
    }

    public function testAssertFalse()
    {
        $this->assertFalse(false);
    }

    public function testAssertTrue()
    {
        $this->assertTrue(true);
    }

    public function testAssertCount()
    {
        $expectedCount = 3;
        $haystack = [2, 5, 4];
        $this->assertCount($expectedCount, $haystack);
    }

    public function testAssertContains()
    {
        $needle = 3;
        $haystack = [2, 5, 3];
        $this->assertContains($needle, $haystack);
    }

    public function testAssertStringContainsString()
    {
        $searchFor = 'foo';
        $searchIn = 'foo';
        $this->assertStringContainsString($searchFor, $searchIn);
    }

    public function testAssertInstanceOf()
    {
        $this->assertInstanceOf(Exception::class, new Exception);
    }

    public function testAssertArrayHasKey()
    {
        $searchKey = 'baz';
        $searchIn = ['baz' => 'bar'];
        $this->assertArrayHasKey($searchKey, $searchIn);
    }

    public function testAssertDirectoryIsWritable()
    {
        $this->assertDirectoryIsWritable('./tests');
    }

    public function testAssertFileIsWritable()
    {
        $this->assertFileIsWritable('./tests/UsefulAssertionsTest.php');
    }
}
