<?php

use PHPUnit\Framework\TestCase;

class EmailTest extends TestCase
{
    /**
     * @dataProvider emailsProvider
     */
    public function testValidEmail($email)
    {
        $this->assertRegExp('/^.+\@\S+\.\S+$/', $email);
    }

    public function emailsProvider()
    {
        return [
            ['asd@asd.asda'],
            ['asd@asd.asdaa'],
            ['asd@asdaa.kkk'],
        ];
    }

    /**
     * @dataProvider numbersProvider
     */
    public function testMath($a, $b, $expected)
    {
        $result = $a * $b;
        $this->assertEquals($expected, $result);
    }

    public function numbersProvider()
    {
        return [
            [1, 2, 2],
            [2, 2, 4],
            [3, 3, 10],
        ];
    }
}
