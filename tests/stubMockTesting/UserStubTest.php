<?php

use forSubMockTesting\User;
use PHPUnit\Framework\TestCase;

class UserStubTest extends TestCase
{
    public function testCreateUser()
    {
        // $user = new User;

        // Stub Classes: all methods return null by default unless mocked
        // $stub = $this->getMockBuilder(User::class)->getMock();
        // $stub->method('save')->willReturn('fake');

        // $stub = $this->createStub(User::class);

        // setMethods is deprecated
        // $stub = $this->getMockBuilder(User::class)->setMethods(null)->getMock(); 

        /**
         * @var PHPUnit\Framework\MockObject\MockObject|User
         */
        $stub = $this->getMockBuilder(User::class)->disableOriginalConstructor()
            ->onlyMethods(['save'])->getMock();
        $stub->method('save')->willReturn(true);

        $this->assertTrue($stub->createUser('Test', 'email@test.com'));
        $this->assertFalse($stub->createUser('Test', 'email'));
    }
}
